﻿using UnityEngine;
using System.Collections;

public abstract class PublicAttributes{

    public static bool HAS_THE_ROUTE_BEEN_FOUND = false;

    public static bool ROUTE_SEARCH_STARTED = false;

    public static int SPAWN_DOOR_INSTANCE;

    public static bool FIRST_TIME_STARTUP = true;

    public static bool FIRST_SCENE = false;

    public static bool PLAYER_SPAWNED = false;

}
