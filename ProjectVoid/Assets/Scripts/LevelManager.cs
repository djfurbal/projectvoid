﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

    public GameObject[] doorArray;

    private GameStateManager stateManager;

    void Awake()
    {
                   
    }


    void OnEnable()
    {

        Debug.Log("OnEnable called");

        stateManager = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();

        stateManager.setDoorArray(doorArray);

        stateManager.spawnPlayer();

    }
    // Use this for initialization
	void Start () {
                        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

  
    public GameObject[] getDoorArray()
    {
        return doorArray;
    }
}
