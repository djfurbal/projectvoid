﻿using UnityEngine;
using System.Collections;

public class doorScript : InteractionScript {

    private GameStateManager stateManager;

    private Vector2 playerLocation, doorPosition;

    private GameObject doorChild;

    private GameObject player;

    private MovementScript move;

    private bool isOpen;

    public int goToLevel;

    public int doorObjective;

    public int doorInstance;

    public bool alwaysOpen;

    public int connectedDoorInstance;

    private bool playerNotFound = false;

    //For optimization
    private Transform playerTransform;

	// Use this for initialization
	void Start () {

        stateManager = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
        doorChild = this.transform.GetChild(0).gameObject;


        player = GameObject.Find("Player");

        if (player == null)
        {
            playerNotFound = true;
        }


        move = player.GetComponent<MovementScript>();
        playerTransform = player.transform;
	}
	
	// Update is called once per frame
	void Update () {


        if (playerNotFound)
        {
            player = GameObject.Find("Player");


            if (!(player == null))
            {
                move = player.GetComponent<MovementScript>();
                playerTransform = player.transform;
                playerNotFound = false;
            }

        }

        playerLocation = playerTransform.position;
        doorPosition = transform.position;

        if (checkPlayerLocation() && connectedDoorInstance != doorInstance)
        {
            Debug.Log("Go to level");
            PublicAttributes.SPAWN_DOOR_INSTANCE = connectedDoorInstance;
            PublicAttributes.PLAYER_SPAWNED = false;
            Application.LoadLevel(goToLevel);
        }
        
        if ((checkState(doorObjective)) || (doorObjective == 0))
        {
            setState(true);
        }
	}

    override public void Interact()
    {
        if (checkPlayerLocation() && (isOpen || alwaysOpen))
        {
            Debug.Log("Go to a Level");
            PublicAttributes.SPAWN_DOOR_INSTANCE = connectedDoorInstance;
            Application.LoadLevel(goToLevel);

        }
        else if (!checkPlayerLocation())
        {
            move.SetMovementTarget(doorChild);
        }
    }


    // returns true if player close enough the door, for the level change
    bool checkPlayerLocation()
    {
        if (doorPosition == playerLocation)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void setState(bool open)
    {
        isOpen = open;
    }

    bool checkState(int objectiveNumber)
    {
        return stateManager.getObjective(objectiveNumber);
    }

    public int getInstace()
    {
        return this.doorInstance;
    }
}
  