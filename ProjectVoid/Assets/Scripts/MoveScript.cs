﻿using UnityEngine;
using System.Collections;

public class MoveScript : InteractionScript {

    private GameObject player;

    private MovementScript move;

    private GameObject moveAreaChild;

    private bool playerNotFound;

	// Use this for initialization
	void Start () {

        player = GameObject.Find("Player");

        if (player == null)
        {
            playerNotFound = true;
        }
        move = player.GetComponent<MovementScript>();
        moveAreaChild = transform.GetChild(0).gameObject;

	}
	
	// Update is called once per frame
	void Update () {

        if (playerNotFound)
        {
            player = GameObject.Find("Player");
            

            if (!(player == null))
            {
                move = player.GetComponent<MovementScript>();
                playerNotFound = false;
            }
        }
	}

    public override void Interact()
    {
        move.SetMovementTarget(moveAreaChild);
        Debug.Log("Movement target set");
    }
}
