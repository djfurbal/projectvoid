﻿using UnityEngine;
using System.Collections;

public class StairScript : InteractionScript {

    private Transform stairPosition;

    private GameObject player;

    private MovementScript move;

	// Use this for initialization
	void Start () {
        
        //Getting stair position, player position and movement script
        stairPosition = transform;
        player = GameObject.Find("Player");
        move = player.GetComponent<MovementScript>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void Interact()
    {
        move.SetMovementTarget(stairPosition.transform.GetChild(0).gameObject);
    }

}
