﻿using UnityEngine;
using System.Collections;

public class ElevatorScript : InteractionScript {

    public float platformSpeed;

    private GameObject player;

    private GameObject platform;

    private MovementScript move;

    private bool isPlayerOnPlatform = false;

    private Vector2 platformUpPosition;

    private Vector2 platformDownPosition;

    private Vector2 playerUpPosition;

    private Vector2 playerDownPosition;

    private Vector2 currPos, playerPos;

    private bool platformOnline = false;

    private bool goingUp = false;

    private bool goingDown = false;

    private bool isItUp = false;

    private bool playerNotFound = false;

	// Use this for initialization
	void Start () {

        player = GameObject.Find("Player");

        if (player == null)
        {
            playerNotFound = true;
        }


        platform = gameObject;
        move = player.GetComponent<MovementScript>();

        platformDownPosition = transform.GetChild(0).position;
        platformUpPosition = transform.GetChild(1).position;
        playerUpPosition = transform.GetChild(2).position;
        playerDownPosition = transform.GetChild(3).position;

	}
	
	// Update is called once per frame
	void Update () {

        if (playerNotFound)
        {
            player = GameObject.Find("Player");


            if (!(player == null))
            {
                move = player.GetComponent<MovementScript>();
                playerNotFound = false;
            }

        }
        currPos = transform.position;
        playerPos = player.transform.position;
        

        if (platformOnline && goingUp && !isItUp)
        {
            if (!Mathf.Approximately(currPos.magnitude, platformUpPosition.magnitude))
            {
                platform.transform.position = Vector2.Lerp(currPos, platformUpPosition, 1 / (platformSpeed * (Vector2.Distance(currPos, platformUpPosition))));
                player.transform.position = Vector2.Lerp(playerPos, playerUpPosition, 1 / (platformSpeed * (Vector2.Distance(playerPos, playerUpPosition)))); 
            }
            else
            {
                platformOnline = false;
                goingUp = false;
                isItUp = true;
                PublicAttributes.ROUTE_SEARCH_STARTED = false;
            }
        }
        else if (platformOnline && goingDown && isItUp)
        {
            if (!Mathf.Approximately(currPos.magnitude, platformDownPosition.magnitude))
            {
                platform.transform.position = Vector2.Lerp(currPos, platformDownPosition, 1 / (platformSpeed * (Vector2.Distance(currPos, platformDownPosition))));
                player.transform.position = Vector2.Lerp(playerPos, playerDownPosition, 1 / (platformSpeed * (Vector2.Distance(playerPos, playerDownPosition))));
            }
            else
            {
                platformOnline = false;
                goingDown = false;
                isItUp = false;
                PublicAttributes.ROUTE_SEARCH_STARTED = false;
            }
        }
	
	}

    public override void Interact()
    {

        Debug.Log("Is player on platform?: " + isPlayerOnPlatform + " Is it up?: " + isItUp);
        if (isPlayerOnPlatform)
        {
            if (isItUp)
            {
                PublicAttributes.ROUTE_SEARCH_STARTED = true;
                platformDown();
                

            }
            else if (!isItUp)
            {
                PublicAttributes.ROUTE_SEARCH_STARTED = true;
                platformUp();
            }
            
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Collision on the platform: " + other.name);

        if (other.name.Equals("Player"))
        {
            isPlayerOnPlatform = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {

        Debug.Log("Collision has left the platform: " + other.name);

        if (other.name.Equals("Player"))
        {
            isPlayerOnPlatform = false;
        }
    }

    void platformUp()
    {
        platformOnline = true;
        goingUp = true;
    }

    void platformDown()
    {
        platformOnline = true;
        goingDown = true;
    }
}
