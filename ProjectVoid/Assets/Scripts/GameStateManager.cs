﻿using UnityEngine;
using System.Collections;

public class GameStateManager : MonoBehaviour {

    private bool objective1 = false
               , objective2 = false
               , objective3 = false
               , objective4 = false;

    public static GameStateManager Instance;

    private Vector3 spawnPointPosition;

    public GameObject player;

    private GameObject newPlayer;

    private int lastLoadedLevel;

    private GameObject[] doorArray;

    private LevelManager levelManager;

    private bool initializationDone = false;

    void Awake()
    {
        if (Instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }

        if (PublicAttributes.FIRST_TIME_STARTUP && Application.loadedLevel == 0)
        {
            newPlayer = Instantiate(player, spawnPointPosition, Quaternion.identity) as GameObject;
            newPlayer.name = "Player";
            PublicAttributes.FIRST_TIME_STARTUP = false;
            PublicAttributes.PLAYER_SPAWNED = true;


        }
    }

	// Use this for initialization
	void Start () {

        lastLoadedLevel = Application.loadedLevel;
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        doorArray = levelManager.getDoorArray();
        spawnPointPosition = GameObject.Find("SpawnPoint").transform.position;
        Debug.Log("get saves");
       
	}
	
	// Update is called once per frame
	void Update () {

        Debug.Log("Player Spawned: " + PublicAttributes.PLAYER_SPAWNED);
        Debug.Log("First time Startup: " + PublicAttributes.FIRST_TIME_STARTUP);

        if (Application.loadedLevel != lastLoadedLevel)
        {
            lastLoadedLevel = Application.loadedLevel;
            initializationDone = false;

            if(!initializationDone) {
            
                levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
                doorArray = levelManager.getDoorArray();
                initializationDone = true;
            }            
        }	
	}

    public void setObjective(bool state, int objectiveNumber)
    {
        switch (objectiveNumber)
        {
            case 1:
                objective1 = state;
                break;

            case 2:
                objective2 = state;
                break;

            case 3:
                objective3 = state;
                break;

            case 4:
                objective4 = state;
                break;
        }
    }

    public bool getObjective(int objectiveNumber)
    {
        switch (objectiveNumber)
        {

            case 1:
                return objective1;
                break;

            case 2:
                return objective2;
                break;

            case 3:
                return objective3;
                break;

            case 4:
                return objective4;
                break;

            default:
                Debug.Log("Value not in the objective range!!");
                return false;
                break;
                
        }
    }

    public void spawnPlayer()
    {
        int spawnindex = PublicAttributes.SPAWN_DOOR_INSTANCE;

        if (!PublicAttributes.PLAYER_SPAWNED && !PublicAttributes.FIRST_TIME_STARTUP)
        {
            for (int i = 0; i < doorArray.Length; i++)
            {
                if (doorArray[i].name.Contains("Instance" + spawnindex))
                {
                    newPlayer = Instantiate(player, doorArray[i].transform.position, Quaternion.identity) as GameObject;
                    newPlayer.name = "Player";
                    PublicAttributes.PLAYER_SPAWNED = true;
                }
            }
        }
    }

    public void setDoorArray(GameObject[] array)
    {
        doorArray = array;
    }
}
