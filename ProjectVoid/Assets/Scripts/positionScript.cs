﻿using UnityEngine;
using System.Collections;

public class positionScript : MonoBehaviour {

    private Vector2 childPosition;

	// Use this for initialization
	void Start () {

       Transform childTransform = this.gameObject.transform.GetChild(0);
       childPosition = childTransform.position;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public Vector2 getPosition()
    {
        return childPosition;
    }
}
