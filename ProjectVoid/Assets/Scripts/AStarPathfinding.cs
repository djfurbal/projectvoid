﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class AStarPathfinding : MonoBehaviour {

    private GameObject[] MovementAreaArray;

    private GameObject[] openList, closedList;

    private GameObject[] FastestRouteArray;

    private List<GameObject> fastestRouteList;

    private GameObject firstObject;

    private Transform playerTransform;

    private Vector2 playerPosition;

    private GameObject currentWaypoint;

    private GameObject[] neighbours = new GameObject[4];

    private bool playerNotFound;

    // Use this for initialization
    void Start () {

        playerTransform = GameObject.Find("Player").transform;

        if (playerTransform == null)
        {
            playerNotFound = true;
        }

        MovementAreaArray = GameObject.FindGameObjectsWithTag("waypoint");
                      
        openList = MovementAreaArray;
        
        fastestRouteList = new List<GameObject>();
        

    }

	// Update is called once per frame
	void Update () {

        playerPosition = playerTransform.position;

        if (playerNotFound)
        {
            playerTransform = GameObject.Find("Player").transform;

            if (!(playerTransform == null))
            {
                playerNotFound = false;
            }
        }

	}

    void FindFastestRoute(GameObject targetArea)
    {
        //Get the position of the player
        firstObject = FindTheClosest();
        currentWaypoint = firstObject;
        GameObject[] neighbours;
        GameObject cheapestNeighbour = null;
        bool routeFound = false;
        int referenceInstance = targetArea.GetInstanceID();
        

        int currentCostOfTheRoute = 0;
        float minHeuristicValue = Mathf.Infinity;
        int loopIndex = 0;

        Debug.Log("going into while loop");
     

        while (!(routeFound))
        {
            if (firstObject == targetArea)
            {
                Debug.Log("First object = target area");
                routeFound = true;
                break;
            }

            //Debug.Log("Get all neighbours");
            //get all neighbours
            neighbours = FindNeighbours(currentWaypoint);


            //value heuristics
            for (int i = 0; i < 4; i++)
            {
              //  Debug.Log("Getting heuristics");
                //right
                if (i == 0 && neighbours[0] != null)
                {
                    minHeuristicValue = GetHeuristicValue(neighbours[0], targetArea);
                    cheapestNeighbour = neighbours[0];

                    Debug.Log("Heuristic value right: " + minHeuristicValue);
                }

                //left
                else if (i == 1 && (minHeuristicValue > GetHeuristicValue(neighbours[1], targetArea)) && neighbours[1] != null)
                {
                    minHeuristicValue = GetHeuristicValue(neighbours[1], targetArea);
                    cheapestNeighbour = neighbours[1];
                    Debug.Log("Heuristic value left: " + minHeuristicValue);
                }

                //up
                else if (i == 2 && (minHeuristicValue > GetHeuristicValue(neighbours[2], targetArea)) && neighbours[2] != null)
                {
                    minHeuristicValue = GetHeuristicValue(neighbours[2], targetArea);
                    cheapestNeighbour = neighbours[2];
                    Debug.Log("Heuristic value up: " + minHeuristicValue);
                }

                //down
                else if (i == 3 && (minHeuristicValue > GetHeuristicValue(neighbours[3], targetArea)) && neighbours[3] != null)
                {

                    minHeuristicValue = GetHeuristicValue(neighbours[3], targetArea);
                    cheapestNeighbour = neighbours[3];
                    Debug.Log("Heuristic value down: " + minHeuristicValue);

                }
                                            
            }

            currentWaypoint = cheapestNeighbour.transform.GetChild(0).gameObject;
            //currentWaypoint = cheapestNeighbour.transform.gameObject;

            //Debug.Log("CheapestNeighbour: " + cheapestNeighbour);

            //FastestRouteArray[currentCostOfTheRoute] = cheapestNeighbour;
            fastestRouteList.Add(cheapestNeighbour.transform.GetChild(0).gameObject);
            currentCostOfTheRoute++;
            Debug.Log(currentCostOfTheRoute);

            if (currentWaypoint.GetInstanceID() == referenceInstance) 
            {
                Debug.Log("Route Found!");
                routeFound = true;
                PublicAttributes.HAS_THE_ROUTE_BEEN_FOUND = true;
                PublicAttributes.ROUTE_SEARCH_STARTED = false;
                break;

            } else if (currentCostOfTheRoute > fastestRouteList.Count) 
            {
                Debug.Log("Weirdness");
                break;

            }
            else
            {
                Array.Clear(neighbours, 0, neighbours.Length);
            }

            loopIndex++;

            if (loopIndex > MovementAreaArray.Length)
            {
                Debug.Log("UNLIMITED LOOP FOUND!");
                Array.Clear(neighbours, 0, neighbours.Length);
                PublicAttributes.ROUTE_SEARCH_STARTED = false;
                break;

            }

        }
    }



    public GameObject[] GetRoute(GameObject targetArea)
    {
        FindFastestRoute(targetArea);
        FastestRouteArray = fastestRouteList.ToArray();
        return FastestRouteArray;
    }

    GameObject FindTheClosest()
    {

        GameObject closestObject = MovementAreaArray[0];
        Vector2    newObjectPosition;

        //Go trough the array to find the closest
        for (int i = 0; i < MovementAreaArray.Length; i++)
        {
                if (i>0) {

                    newObjectPosition = MovementAreaArray[i].transform.position;
              //      Debug.Log("MovementAreaArray gameobject name: " + MovementAreaArray[i].name);    


                    if((Vector2.Distance(playerPosition, closestObject.transform.position)) > (Vector2.Distance(playerPosition, newObjectPosition))) {
                        closestObject = MovementAreaArray[i];
                }
            }

        }

        Debug.Log("CLOSEST OBJECT!: " + closestObject.name);
        return closestObject;
    }

    GameObject[] FindNeighbours(GameObject currentGameobject)
    {

        //Debug.Log("Getting all neighbours");
        RaycastHit2D neighbourHit;
      try {

            Debug.DrawRay(currentGameobject.transform.position, Vector2.right * 2f, Color.green,20f);
            //raycast right, get the gameobject
            neighbourHit = Physics2D.Raycast(currentGameobject.transform.position, Vector2.right, 2f, 9);
            neighbours[0] = neighbourHit.transform.gameObject;
        } catch(NullReferenceException e) {
          //  Debug.Log("Neigbour right equals NULL!");
            neighbours[0] = null;
        }
        try { 
            Debug.DrawRay(currentGameobject.transform.position, Vector2.left * 2f, Color.red,20f);
            //raycast left, get the gameobject
            neighbourHit = Physics2D.Raycast(currentGameobject.transform.position, Vector2.left, 2f, 9);
            neighbours[1] = neighbourHit.transform.gameObject;
        } catch(NullReferenceException e) {
            neighbours[1] = null;
            //Debug.Log("Neigbour left equals NULL!");
        }
        try {
            Debug.DrawRay(currentGameobject.transform.position, Vector2.up * 2f, Color.blue, 20f);
            //raycast up, get the gameobject
            neighbourHit = Physics2D.Raycast(currentGameobject.transform.position, Vector2.up, 2f, 9);
            neighbours[2] = neighbourHit.transform.gameObject;
        } catch(NullReferenceException e) {
            neighbours[2] = null;
            //Debug.Log("Neigbour up equals NULL!");
        }
        try {
            Debug.DrawRay(currentGameobject.transform.position, Vector2.down * 2f, Color.yellow, 20f);
            //raycast down, get the gameobject
            neighbourHit = Physics2D.Raycast(currentGameobject.transform.position, Vector2.down, 2f, 9);
            neighbours[3] = neighbourHit.transform.gameObject;
        }
        catch (NullReferenceException e)
        {
            neighbours[3] = null;
            //Debug.Log("Neigbour down equals NULL!");
        }
            //Debug.Log("Neighbour array length: " + neighbours.Length);
            return neighbours;
    }

    float GetHeuristicValue(GameObject currentObj, GameObject targetObj)
    {

        //Debug.Log("Getting heuristics for: " + currentObj + " vs. " + targetObj);

        if (!(currentObj == null))
        {
            Vector2 offset = targetObj.transform.position - currentObj.transform.position;
            float value = offset.sqrMagnitude;

            Debug.Log("Heuristic value for: " + currentObj + " vs. " + targetObj + " is " + value);

            return value;
        }

        return Mathf.Infinity;
;
    }

    void LateUpdate()
    {
        //Debug.Log("Has the route been found?: " + PublicAttributes.HAS_THE_ROUTE_BEEN_FOUND);
        if(PublicAttributes.HAS_THE_ROUTE_BEEN_FOUND)
        {
            fastestRouteList.Clear();
            Debug.Log("FastestRouteList Cleared, size: " + fastestRouteList.Count);
        }
    }
}
