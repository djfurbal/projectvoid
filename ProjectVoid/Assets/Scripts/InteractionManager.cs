﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class InteractionManager : MonoBehaviour {

    private GameObject player;

    private Transform fingerLocationTransform;

    private Vector2 curLocation;

    private Collider2D coll;

    private InteractionScript interaction;

	// Use this for initialization
	void Start () {

        fingerLocationTransform = transform;
        
	
	}
	
	// Update is called once per frame
	void Update () {

        curLocation = transform.position;

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began &&
            EventSystem.current.currentSelectedGameObject == null)
        {
            //Catching the touch
            Touch touch = Input.GetTouch(0);

            //Catching ray and raycastHit
            //RaycastHit2D hit;

            Vector2 worldTouch = Camera.main.ScreenToWorldPoint(touch.position);

            //Catching the V2 position of the touch/mouse and converting it to match the worlpoint

            //if we use editor
            #if UNITY_EDITOR
            Debug.DrawLine(curLocation, worldTouch, Color.black);
            
            //if we use touch device
            #elif (UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8)
            ray = Camera.main.ScreenPointToRay(worldTouch);

            #endif
            //Setting position to touch
            transform.position = worldTouch;
                        
        }

	}

    void OnTriggerEnter2D(Collider2D other) {

        //Debug.Log("OnTriggerEnter2D name: " + other.name);
        //Debug.Log("OnTriggerEnter2D tag : " + other.tag);

        if (other.tag.Equals("Interactable"))
        {

            interaction = other.gameObject.GetComponent<InteractionScript>();
            Debug.Log("Interaction Script: " + interaction);
            DoInteraction();
        }

    }

    void DoInteraction()
    {
        interaction.Interact();
        //Debug.Log("Doing the interaction");
    }
}
