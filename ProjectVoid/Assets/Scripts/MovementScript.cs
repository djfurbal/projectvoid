﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class MovementScript : MonoBehaviour {

    private float movSpeed = 15F;

    private Vector2 movTarget;

    private Vector2 currentPos;

    private Transform rayStart;

    private GameObject movementBox;

    private AStarPathfinding aStar;

    private GameObject star;

    private GameObject[] route;

    private int index;

    bool routeCompleted = false;


    // Use this for initialization
    void Start () {

        star = GameObject.Find("AStarPathfinding");
        aStar = star.GetComponent<AStarPathfinding>();
        rayStart = transform.GetChild(0);
        index = 0;
        

    }
	
	// Update is called once per frame
	void Update () {

        currentPos = transform.position;
        //Debug.Log("Movement status: " + movementStarted);

        
        if ( PublicAttributes.HAS_THE_ROUTE_BEEN_FOUND)
        {

              
            // Debug.Log("index: " + index);
            // Debug.Log("Route length: " + route.Length);
            
            if (!Mathf.Approximately(currentPos.magnitude, route[index].transform.position.magnitude))
            {
                //move the gameobject to the desired position
                gameObject.transform.position = Vector2.Lerp(transform.position, route[index].transform.position, 1 / (movSpeed * (Vector2.Distance(gameObject.transform.position, route[index].transform.position))));
                //Debug.Log("DONT STOP ME NOW!");

                Debug.Log("Current route target: " + route[index].transform.gameObject.name);

                //Play walk animation

            }

            else if (Mathf.Approximately(currentPos.magnitude, route[index].transform.position.magnitude) && !routeCompleted)
            {

                //stop walk animation, start idle
                if (index < route.Length)
                {
                    index++;
                }
            }

            if (index >= route.Length)
            {
                Debug.Log("Index reset");
                routeCompleted = true;
                PublicAttributes.HAS_THE_ROUTE_BEEN_FOUND = false;
                PublicAttributes.ROUTE_SEARCH_STARTED = false;
                index = 0;
            }
        }
    }

    public void SetMovementTarget(GameObject target)
    {

        Debug.Log("Route Search Started: " + PublicAttributes.ROUTE_SEARCH_STARTED);

        if (!PublicAttributes.ROUTE_SEARCH_STARTED)
        {
            PublicAttributes.ROUTE_SEARCH_STARTED = true;
            routeCompleted = false;
            route = aStar.GetRoute(target);
            
            
        }
        

        Debug.Log("target set at: " + target);
    }

    
}
